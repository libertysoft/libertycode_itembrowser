<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/operation/library/ConstOperatorData.php');
include($strRootPath . '/src/operation/exception/PathInvalidFormatException.php');
include($strRootPath . '/src/operation/exception/PathConfigNotFoundException.php');
include($strRootPath . '/src/operation/model/OperatorData.php');

include($strRootPath . '/src/browser/library/ConstBrowser.php');
include($strRootPath . '/src/browser/exception/RegisterInvalidFormatException.php');
include($strRootPath . '/src/browser/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/browser/api/BrowserInterface.php');
include($strRootPath . '/src/browser/model/DefaultBrowser.php');

include($strRootPath . '/src/browser/page/library/ConstPageBrowser.php');
include($strRootPath . '/src/browser/page/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/browser/page/api/PageBrowserInterface.php');
include($strRootPath . '/src/browser/page/model/PageBrowser.php');

include($strRootPath . '/src/browser/operation/library/ConstOperatorBrowser.php');
include($strRootPath . '/src/browser/operation/exception/OperationTypeInvalidFormatException.php');
include($strRootPath . '/src/browser/operation/exception/OperationConfigInvalidFormatException.php');
include($strRootPath . '/src/browser/operation/exception/OperationValueInvalidFormatException.php');
include($strRootPath . '/src/browser/operation/api/OperatorBrowserInterface.php');
include($strRootPath . '/src/browser/operation/model/OperatorBrowser.php');

include($strRootPath . '/src/browser/operation/auto/library/ConstAutoOperatorBrowser.php');
include($strRootPath . '/src/browser/operation/auto/model/AutoOperatorBrowser.php');

include($strRootPath . '/src/browser/operation/sort/library/ConstSortBrowser.php');
include($strRootPath . '/src/browser/operation/sort/model/SortBrowser.php');

include($strRootPath . '/src/browser/operation/criteria/library/ConstCriteriaBrowser.php');
include($strRootPath . '/src/browser/operation/criteria/model/CriteriaBrowser.php');