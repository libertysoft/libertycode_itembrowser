<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\page\library;



class ConstPageBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE = 'item_count_per_page';
    const TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX = 'active_page_index';

    // Cache configuration
    const TAB_CACHE_CONFIG_KEY_PAGE_COUNT = 'page_count';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the page browser configuration standard.';



}