<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\page\exception;

use liberty_code\item_browser\browser\page\library\ConstPageBrowser;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPageBrowser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid item count per page
            (
                (!isset($config[ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE])) ||
                (
                    is_int($config[ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE]) &&
                    ($config[ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE] >= 0)
                )
            ) &&

            // Check valid active page index
            (
                (!isset($config[ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX])) ||
                (
                    is_int($config[ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX]) &&
                    ($config[ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX] >= 0)
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            (is_array($config) && (count($config) > 0)) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}