<?php
/**
 * Description :
 * This class allows to describe behavior of page browser class.
 * Page browser is browser, that uses page browsing configuration,
 * to provide paginated result set of items.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\page\api;

use liberty_code\item_browser\browser\api\BrowserInterface;



interface PageBrowserInterface extends BrowserInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get count of item, per page.
     *
     * @return integer
     */
    public function getIntItemCountPerPage();



    /**
     * Get count of item, on active page.
     *
     * @return integer
     */
    public function getIntItemCountOnActivePage();



    /**
     * Get total count of page.
     *
     * @return integer
     */
    public function getIntPageCount();



    /**
     * Get index of active page.
     *
     * @return integer
     */
    public function getIntActivePageIndex();





    // Methods setters
    // ******************************************************************************

    /**
     * Set count of item, per page.
     *
     * @param integer $intCount
     */
    public function setItemCountPerPage($intCount);



    /**
     * Set index of active page.
     *
     * @param integer $intIndex
     */
    public function setActivePageIndex($intIndex);
}