<?php
/**
 * Description :
 * This class allows to define page browser class.
 * Can be consider is base of all page browser type.
 * Page browser uses the following specified browsing configuration:
 * [
 *     Default browser configuration,
 *
 *     item_count_per_page(optional: got 0 if item_count_per_page not found):
 *         "positive integer represents number items per page (0 means all items per page, so only one page required)",
 *
 *     active_page_index(optional: got 0 if active_page_index not found):
 *         "positive integer represents active page selected, for items providing (must be between 0 and < page count)"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\page\model;

use liberty_code\item_browser\browser\model\DefaultBrowser;
use liberty_code\item_browser\browser\page\api\PageBrowserInterface;

use liberty_code\item_browser\browser\library\ConstBrowser;
use liberty_code\item_browser\browser\page\library\ConstPageBrowser;
use liberty_code\item_browser\browser\page\exception\ConfigInvalidFormatException;



abstract class PageBrowser extends DefaultBrowser implements PageBrowserInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstBrowser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getIntItemCountPerPage()
    {
        // Init var
        $result = 0;
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE, $tabConfig))
        {
            $result = $tabConfig[ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntItemCountOnActivePage()
    {
        // Init var
        $intItemCount = $this->getIntItemCount();
        $intItemCountPerPage = $this->getIntItemCountPerPage();
        $intPageCount = $this->getIntPageCount();
        $intActivePageIndex = $this->getIntActivePageIndex();
        $intLastPageIndex = (($intPageCount > 0) ? ($intPageCount - 1) : 0);
        $result = $intItemCountPerPage;

        // Case 0 or 1 page
        if(($intPageCount == 0) || ($intPageCount == 1))
        {
            $result = $intItemCount;
        }
        // Case last page
        else if($intActivePageIndex == $intLastPageIndex)
        {
            $intRestDiv = ($intItemCount % $intItemCountPerPage);
            if($intRestDiv > 0) $result = $intRestDiv;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntPageCount()
    {
        // Init var
        $result = $this->getCacheItem(ConstPageBrowser::TAB_CACHE_CONFIG_KEY_PAGE_COUNT);

        // Calculate count, if not found in cache
        if(is_null($result))
        {
            // Get info
            $intItemCount = $this->getIntItemCount();
            $intItemCountPerPage = $this->getIntItemCountPerPage();

            // Calculate page count
            $result = 1;
            if($intItemCountPerPage > 0)
            {
                $result = intdiv($intItemCount, $intItemCountPerPage);
                $intRestDiv = $intItemCount % $intItemCountPerPage;
                if($intRestDiv > 0) $result++;
            }

            // Register in cache
            $this->putCacheItem(ConstPageBrowser::TAB_CACHE_CONFIG_KEY_PAGE_COUNT, $result);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntActivePageIndex()
    {
        // Init var
        $result = 0;
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX, $tabConfig))
        {
            $result = $tabConfig[ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX];
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setItemCountPerPage($intCount)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set item count per page
        $tabConfig[ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE] = $intCount;

        // Set configuration
        $this->setTabConfig($tabConfig);
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setActivePageIndex($intIndex)
    {
        // Init var
        $intPageCount = $this->getIntPageCount();
        $tabConfig = $this->getTabConfig();

        // Set item count per page
        $tabConfig[ConstPageBrowser::TAB_CONFIG_KEY_ACTIVE_PAGE_INDEX] = $intIndex;

        // Check index not over page count
        if(
            is_int($intIndex) &&
            ($intIndex >= $intPageCount) &&
            ($intIndex > 0)
        )
        {
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Set configuration
        $this->setTabConfig($tabConfig, false);
    }





    // Methods cache
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabCacheKey()
    {
        // Init var
        $result = parent::getTabCacheKey();
        $result[] = ConstPageBrowser::TAB_CACHE_CONFIG_KEY_PAGE_COUNT;

        // Return result
        return $result;
    }



}