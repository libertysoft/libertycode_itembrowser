<?php
/**
 * Description :
 * This class allows to define default browser class.
 * It use register to cache calculated information.
 * Can be consider is base of all browser type.
 * Default browser uses the following specified browsing configuration:
 * [
 *     query(required): "mixed selection query"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\item_browser\browser\api\BrowserInterface;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\item_browser\browser\library\ConstBrowser;
use liberty_code\item_browser\browser\exception\RegisterInvalidFormatException;
use liberty_code\item_browser\browser\exception\ConfigInvalidFormatException;



/**
 * @method null|RegisterInterface getObjCacheRegister() Get cache register object.
 * @method array getTabConfig() Get configuration array.
 * @method void setObjCacheRegister(null|RegisterInterface $objCacheRegister) Set cache register object.
 */
abstract class DefaultBrowser extends FixBean implements BrowserInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param RegisterInterface $objCacheRegister = null
	 * @param array $tabConfig = null
     */
	public function __construct(
        RegisterInterface $objCacheRegister = null,
	    array $tabConfig = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init cache register if required
        if(!is_null($objCacheRegister))
        {
            $this->setObjCacheRegister($objCacheRegister);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBrowser::DATA_KEY_DEFAULT_CACHE_REGISTER))
        {
            $this->__beanTabData[ConstBrowser::DATA_KEY_DEFAULT_CACHE_REGISTER] = null;
        }

        if(!$this->beanExists(ConstBrowser::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstBrowser::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstBrowser::DATA_KEY_DEFAULT_CACHE_REGISTER,
            ConstBrowser::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstBrowser::DATA_KEY_DEFAULT_CACHE_REGISTER:
                    RegisterInvalidFormatException::setCheck($value);
                    break;

                case ConstBrowser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getQuery()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstBrowser::TAB_CONFIG_KEY_QUERY];

        // Return result
        return $result;
    }



    /**
     * Get total count of item.
     *
     * @return integer
     */
    abstract protected function getIntItemCountEngine();




    /**
     * @inheritdoc
     */
    public function getIntItemCount()
    {
        // Init var
        $result = $this->getCacheItem(ConstBrowser::TAB_CACHE_CONFIG_KEY_ITEM_COUNT);

        // Calculate count, if not found in cache
        if(is_null($result))
        {
            $result = $this->getIntItemCountEngine();

            // Register in cache
            $this->putCacheItem(ConstBrowser::TAB_CACHE_CONFIG_KEY_ITEM_COUNT, $result);
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setQuery($query)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set query
        $tabConfig[ConstBrowser::TAB_CONFIG_KEY_QUERY] = $query;

        // Set configuration
        $this->setTabConfig($tabConfig);
    }



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @param boolean $boolCacheClear = true
     * @throws ConfigInvalidFormatException
     */
    public function setTabConfig(array $tabConfig, $boolCacheClear = true)
    {
        // Init var
        $boolCacheClear = (is_bool($boolCacheClear) ? $boolCacheClear : true);

        // Set configuration
        $this->beanSet(ConstBrowser::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Clear all cache, if required
        if($boolCacheClear)
        {
            $this->removeCacheAll();
        }
    }





    // Methods cache
    // ******************************************************************************

    /**
     * Get string cache hash.
     *
     * @return string
     */
    protected function getStrCacheHash()
    {
        // Return result
        return ConstBrowser::CONF_CACHE_HASH_PREFIX . spl_object_hash($this);
    }



    /**
     * Get string cache key formatted,
     * from specified cache key.
     *
     * @param string $strKey
     * @return string
     */
    protected function getStrCacheKey($strKey)
    {
        // Return result
        return sprintf(
            ConstBrowser::CONF_CACHE_KEY_PATTERN,
            $this->getStrCacheHash(),
            $strKey
        );
    }



    /**
     * Get index array of cache keys.
     *
     * @return array
     */
    protected function getTabCacheKey()
    {
        // Return result
        return array(
            ConstBrowser::TAB_CACHE_CONFIG_KEY_ITEM_COUNT
        );
    }



    /**
     * Get cache item.
     *
     * @param string $strKey
     * @return null|mixed
     */
    protected function getCacheItem($strKey)
    {
        // Init var
        $result = null;
        $tabKey = $this->getTabCacheKey();
        $objRegister = $this->getObjCacheRegister();

        // Get item from cache, if required
        if(
            (!is_null($objRegister)) &&
            (in_array($strKey, $tabKey))
        )
        {
            $strKey = $this->getStrCacheKey($strKey);
            $result = $objRegister->getItem($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Put a specified cache item (update if exists, add else).
     *
     * @param string $strKey
     * @param mixed $item
     */
    protected function putCacheItem($strKey, $item)
    {
        // Init var
        $tabKey = $this->getTabCacheKey();
        $objRegister = $this->getObjCacheRegister();

        // Get item from cache, if required
        if(
            (!is_null($objRegister)) &&
            (in_array($strKey, $tabKey))
        )
        {
            $strKey = $this->getStrCacheKey($strKey);
            $objRegister->putItem($strKey, $item);
        }
    }



    /**
     * Remove all cache items.
     */
    public function removeCacheAll()
    {
        // Init var
        $tabKey = $this->getTabCacheKey();
        $objRegister = $this->getObjCacheRegister();

        // Get item from cache, if required
        if(!is_null($objRegister))
        {
            foreach($tabKey as $strKey)
            {
                $strKey = $this->getStrCacheKey($strKey);
                if($objRegister->checkItemExists($strKey))
                {
                    $objRegister->removeItem($strKey);
                }
            }
        }
    }



}