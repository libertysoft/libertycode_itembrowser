<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\library;



class ConstBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CACHE_REGISTER = 'objCacheRegister';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_QUERY = 'query';

    // Cache configuration
    const CONF_CACHE_HASH_PREFIX = 'item_browser_';
    const CONF_CACHE_KEY_PATTERN = '%1$s_%2$s';

    const TAB_CACHE_CONFIG_KEY_ITEM_COUNT = 'item_count';



    // Exception message constants
    const EXCEPT_MSG_CACHE_REGISTER_INVALID_FORMAT = 'Following cache register "%1$s" invalid! The register must be a register object, not null.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the default browser configuration standard.';



}