<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/browser/operation/test/CriteriaBrowserTest.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\library\ConstBrowser;

// Use test
use liberty_code\item_browser\browser\operation\test\CriteriaBrowserTest;



// Init var
$tabConfig = array(
    ConstBrowser::TAB_CONFIG_KEY_QUERY => []
);
$objRegister = new MemoryRegister();
$objOperatorData = new OperatorData();
$objPageBrowser = new CriteriaBrowserTest(
    $objOperatorData,
    $objRegister,
    $tabConfig,
    array(),
    array()
);



// Test get items
$tabConfig = array(
    [
        [
            'key_3' => true,
            'key_4' => 'Value A'
        ],
        0,
        0
    ], // Ok
    [
        [
            'key_3' => true,
            'key_4' => 'Value A'
        ],
        'test',
        0
    ], // Ko: bad format on item count per page
    [
        [],
        50,
        0
    ], // Ok
    [
        [],
        50,
        'test',
    ], // Ko: bad format on active page index
    [
        [
            'key_4' => 'Value B'
        ],
        2,
        0
    ], // Ok
    [
        [
            'key_4' => 'Value B'
        ],
        2,
        2
    ], // Ok
    [
        [
            'key_4' => 'Value B'
        ],
        2,
        3
    ], // Ko: active page index out of bound
    [
        [
            'key_3' => false,
            'key_4' => 'Value B'
        ],
        1,
        1
    ] // Ok
);

foreach($tabConfig as $config)
{
	echo('Test get items: <br />');
	try{
	    $query = $config[0];
        $intItemCountPerPage = $config[1];
        $intActivePageIndex = $config[2];

        // echo('Set: Query: <pre>');var_dump($query);echo('</pre>');
        $objPageBrowser->setQuery($query);

        // echo('Set: Item count per page: <pre>');var_dump($intItemCountPerPage);echo('</pre>');
        $objPageBrowser->setItemCountPerPage($intItemCountPerPage);

        // echo('Set: Active page index: <pre>');var_dump($intActivePageIndex);echo('</pre>');
        $objPageBrowser->setActivePageIndex($intActivePageIndex);

        echo('Get: Query: <pre>');var_dump($objPageBrowser->getQuery());echo('</pre>');
        echo('Get: Item count: <pre>');var_dump($objPageBrowser->getIntItemCount());echo('</pre>');
        echo('Get: Item count per page: <pre>');var_dump($objPageBrowser->getIntItemCountPerPage());echo('</pre>');
        echo('Get: Item count on active page: <pre>');var_dump($objPageBrowser->getIntItemCountOnActivePage());echo('</pre>');
        echo('Get: Page count: <pre>');var_dump($objPageBrowser->getIntPageCount());echo('</pre>');
        echo('Get: Active page index: <pre>');var_dump($objPageBrowser->getIntActivePageIndex());echo('</pre>');

        echo('Get: Items: <pre>');var_dump($objPageBrowser->getTabItem());echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get cache register items
$tabCacheItem = $objPageBrowser->getObjCacheRegister()->getTabItem();
echo('Cache register items: <pre>');var_dump($tabCacheItem);echo('</pre>');

echo('<br /><br /><br />');


