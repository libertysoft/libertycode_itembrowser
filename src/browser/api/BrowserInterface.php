<?php
/**
 * Description :
 * This class allows to describe behavior of browser class.
 * Browser allows to provide items, from specified selection query
 * and from specified browsing configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\api;



interface BrowserInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get selection query.
	 *
	 * @return mixed
	 */
	public function getQuery();



    /**
     * Get total count of item.
     *
     * @return integer
     */
    public function getIntItemCount();



    /**
     * Get index array of items.
     *
     * @return array
     */
    public function getTabItem();





    // Methods setters
    // ******************************************************************************

    /**
     * Set selection query.
     *
     * @param mixed $query
     */
    public function setQuery($query);
}