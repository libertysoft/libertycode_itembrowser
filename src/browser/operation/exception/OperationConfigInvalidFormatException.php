<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\exception;

use liberty_code\item_browser\browser\operation\library\ConstOperatorBrowser;



class OperationConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $type
     * @param mixed $key
	 * @param mixed $config
	 * @param mixed $errorMsg
     */
	public function __construct($type, $key, $config, $errorMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(
				(!is_null($errorMsg)) &&
				is_string($errorMsg) && (trim($errorMsg) != '')
			) ?
				' ' . trim($errorMsg) :
				'';
		$this->message = sprintf(
		    ConstOperatorBrowser::EXCEPT_MSG_OPERATION_CONFIG_INVALID_FORMAT,
            strval($type),
            strval($key),
            strval($config),
            $strErrorMsg
        );
	}
	
	
	
}