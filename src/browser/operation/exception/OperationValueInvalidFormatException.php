<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\exception;

use liberty_code\item_browser\browser\operation\library\ConstOperatorBrowser;



class OperationValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $type
     * @param mixed $key
	 * @param mixed $value
	 * @param mixed $errorMsg
     */
	public function __construct($type, $key, $value, $errorMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(
				(!is_null($errorMsg)) &&
				is_string($errorMsg) && (trim($errorMsg) != '')
			) ?
				' ' . trim($errorMsg) :
				'';
		$this->message = sprintf(
		    ConstOperatorBrowser::EXCEPT_MSG_OPERATION_VALUE_INVALID_FORMAT,
            strval($type),
            strval($key),
            strval($value),
            $strErrorMsg
        );
	}
	
	
	
}