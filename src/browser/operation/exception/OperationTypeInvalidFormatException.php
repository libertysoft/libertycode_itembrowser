<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\exception;

use liberty_code\item_browser\browser\operation\library\ConstOperatorBrowser;



class OperationTypeInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $type
     * @param mixed $errorMsg = null
     */
	public function __construct($type, $errorMsg = null)
	{
		// Call parent constructor
		parent::__construct();

        // Init var
        $strErrorMsg =
            (
                (!is_null($errorMsg)) &&
                is_string($errorMsg) && (trim($errorMsg) != '')
            ) ?
                ' ' . trim($errorMsg) :
                '';
        $this->message = sprintf(
            ConstOperatorBrowser::EXCEPT_MSG_OPERATION_TYPE_INVALID_FORMAT,
            strval($type),
            $strErrorMsg
        );
	}
	
	
	
}