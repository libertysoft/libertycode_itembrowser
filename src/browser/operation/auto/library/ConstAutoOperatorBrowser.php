<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\auto\library;



class ConstAutoOperatorBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Methods configuration
    const CONF_METHOD_REGEXP_HYDRATE_CONFIG = '#^(hydrate)(\w+)(Config)$#';
    const CONF_METHOD_REGEXP_HYDRATE_VALUE = '#^(hydrate)(\w+)(Value)$#';
    const CONF_METHOD_REGEXP_CHECK_CONFIG_EXISTS = '#^(check)(\w+)(ConfigExists)$#';
    const CONF_METHOD_REGEXP_CHECK_VALUE_EXISTS = '#^(check)(\w+)(ValueExists)$#';
    const CONF_METHOD_REGEXP_GET_TAB_CONFIG = '#^(getTab)(\w+)(Config)$#';
    const CONF_METHOD_REGEXP_GET_CONFIG = '#^(get)(\w+)(Config)$#';
    const CONF_METHOD_REGEXP_GET_TAB_VALUE = '#^(getTab)(\w+)(Value)$#';
    const CONF_METHOD_REGEXP_GET_VALUE = '#^(get)(\w+)(Value)$#';
    const CONF_METHOD_REGEXP_SET_CONFIG = '#^(set)(\w+)(Config)$#';
    const CONF_METHOD_REGEXP_SET_VALUE = '#^(set)(\w+)(Value)$#';
    const CONF_METHOD_REGEXP_REMOVE_CONFIG = '#^(remove)(\w+)(Config)$#';
    const CONF_METHOD_REGEXP_REMOVE_CONFIG_ALL = '#^(remove)(\w+)(ConfigAll)$#';
    const CONF_METHOD_REGEXP_REMOVE_VALUE = '#^(remove)(\w+)(Value)$#';
    const CONF_METHOD_REGEXP_REMOVE_VALUE_ALL = '#^(remove)(\w+)(ValueAll)$#';

    const CONF_METHOD_PATTERN_REPLACE = '${1}Operation${3}';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of operation method REGEXPs.
     *
     * @return array
     */
    static public function getTabMethodRegexp()
    {
        // Return result
        return array(
            static::CONF_METHOD_REGEXP_HYDRATE_CONFIG,
            static::CONF_METHOD_REGEXP_HYDRATE_VALUE,
            static::CONF_METHOD_REGEXP_CHECK_CONFIG_EXISTS,
            static::CONF_METHOD_REGEXP_CHECK_VALUE_EXISTS,
            static::CONF_METHOD_REGEXP_GET_TAB_CONFIG,
            static::CONF_METHOD_REGEXP_GET_CONFIG,
            static::CONF_METHOD_REGEXP_GET_TAB_VALUE,
            static::CONF_METHOD_REGEXP_GET_VALUE,
            static::CONF_METHOD_REGEXP_SET_CONFIG,
            static::CONF_METHOD_REGEXP_SET_VALUE,
            static::CONF_METHOD_REGEXP_REMOVE_CONFIG,
            static::CONF_METHOD_REGEXP_REMOVE_CONFIG_ALL,
            static::CONF_METHOD_REGEXP_REMOVE_VALUE,
            static::CONF_METHOD_REGEXP_REMOVE_VALUE_ALL
        );
    }



}