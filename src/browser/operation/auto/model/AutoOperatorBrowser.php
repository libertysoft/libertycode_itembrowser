<?php
/**
 * Description :
 * This class allows to define automatic operator browser class.
 * Automatic operator browser is operator browser,
 * that allows to call customized operation methods, by using valid type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\auto\model;

use liberty_code\item_browser\browser\operation\model\OperatorBrowser;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\item_browser\browser\operation\auto\library\ConstAutoOperatorBrowser;



abstract class AutoOperatorBrowser extends OperatorBrowser
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Magical methods
    // ******************************************************************************

    /**
     * Magical method allows to catch call of unexisting method.
     *
     * @param $strMethodNm
     * @param $tabMethodArg
     * @return object
     */
    /**
     * @inheritdoc
     */
    public function __call($strMethodNm, $tabMethodArg)
    {
        // Init var
        // $result = null;

        // Get operation callable
        $callable = null;
        $tabMethodRegexp = ConstAutoOperatorBrowser::getTabMethodRegexp();
        for($intCpt = 0; ($intCpt < count($tabMethodRegexp)) && (is_null($callable)); $intCpt++)
        {
            // Get info
            $strMethodRegexp = $tabMethodRegexp[$intCpt];

            // Check method name matches, and match return valid
            $tabMatch = array();
            if(
                (preg_match($strMethodRegexp, $strMethodNm, $tabMatch) == 1) &&
                (count($tabMatch) == 4)
            )
            {
                // Get info
                $strType = $this->getStrTypeFromMethodNameType($tabMatch[2]);

                // Check operation type valid
                if($this->checkOperationTypeValid($strType))
                {
                    // Get function name, for operation
                    $strOpeMethodNm = preg_replace(
                        $strMethodRegexp,
                        ConstAutoOperatorBrowser::CONF_METHOD_PATTERN_REPLACE,
                        $strMethodNm
                    );

                    // Get array of arguments, for operation
                    $tabOpeMethodArg = $tabMethodArg;
                    array_unshift(
                        $tabOpeMethodArg,
                        $strType
                    );

                    // Get function callable
                    $callable = ToolBoxReflection::getCallFunction(
                        array($this, $strOpeMethodNm),
                        $tabOpeMethodArg
                    );
                }
            }
        }

        // Case operation callable found: call it
        if(!is_null($callable))
        {
            $result = $callable();
        }
        // Case else: operation callable not found: call parent
        else
        {
            // Call parent method
            $result = parent::__call($strMethodNm, $tabMethodArg);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string type,
     * from specified type used on method name.
     *
     * @param string $strType
     * @return string
     */
    protected function getStrTypeFromMethodNameType($strType)
    {
        // Return result
        return strtolower($strType);
    }



}