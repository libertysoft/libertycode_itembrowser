<?php
/**
 * Description :
 * This class allows to define criteria browser class.
 * Criteria browser is automatic operator browser, that uses specified sort and criteria operations,
 * as browsing configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\criteria\model;

use liberty_code\item_browser\browser\operation\sort\model\SortBrowser;

use liberty_code\item_browser\browser\operation\criteria\library\ConstCriteriaBrowser;



/**
 * @method boolean hydrateCriteriaConfig(array $tabConfig = array(), boolean $boolOverwrite = true, boolean $boolClear = true) Hydrate criteria operation configurations.
 * @see SortBrowser::hydrateOperationConfig()
 *
 * @method boolean hydrateCriteriaValue(array $tabValue = array(), boolean $boolOverwrite = true, boolean $boolClear = true) Hydrate criteria operation values.
 * @see SortBrowser::hydrateOperationValue()
 *
 * @method boolean checkCriteriaConfigExists(string $strKey) Check if the specified criteria operation configuration exists.
 * @see SortBrowser::checkOperationConfigExists()
 *
 * @method boolean checkCriteriaValueExists(string $strKey) Check if the specified criteria operation value exists.
 * @see SortBrowser::checkOperationValueExists()
 *
 * @method array getTabCriteriaConfig() Get all criteria operation configurations.
 * @see SortBrowser::getTabOperationConfig()
 *
 * @method array getCriteriaConfig(string $strKey) Get criteria operation configuration.
 * @see SortBrowser::getOperationConfig()
 *
 * @method array getTabCriteriaValue() Get all criteria operation values.
 * @see SortBrowser::getTabOperationValue()
 *
 * @method array getCriteriaValue(string $strKey) Get criteria operation value.
 * @see SortBrowser::getOperationValue()
 *
 * @method array setCriteriaConfig(string $strKey, mixed $config) Set specified criteria operation configuration.
 * @see SortBrowser::setOperationConfig()
 *
 * @method array setCriteriaValue(string $strKey, mixed $value) Set specified criteria operation value.
 * @see SortBrowser::setOperationValue()
 *
 * @method array removeCriteriaConfig(string $strKey) Remove specified criteria operation configuration.
 * @see SortBrowser::removeOperationConfig()
 *
 * @method array removeCriteriaConfigAll() Remove all criteria operation configurations.
 * @see SortBrowser::removeOperationConfigAll()
 *
 * @method array removeCriteriaValue(string $strKey) Remove specified criteria operation value.
 * @see SortBrowser::removeOperationValue()
 *
 * @method array removeCriteriaValueAll() Remove all criteria operation values.
 * @see SortBrowser::removeOperationValueAll()
 */
abstract class CriteriaBrowser extends SortBrowser
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkOperationTypeValid($strType, &$error = null)
    {
        // Init var
        $result =
            parent::checkOperationTypeValid($strType) ||
            ($strType == ConstCriteriaBrowser::OPERATION_TYPE_CRITERIA);

        // Set error message, if check not pass
        if(!$result)
        {
            $error = ConstCriteriaBrowser::EXCEPT_MSG_OPERATION_TYPE_INVALID_FORMAT;
        }

        // Return result
        return $result;
    }



}