<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\library;



class ConstOperatorBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_OPERATION_TYPE_INVALID_FORMAT = 'Following operation type "%1$s" failed validation!%2$s';
    const EXCEPT_MSG_OPERATION_CONFIG_INVALID_FORMAT = 'Following operation configuration "%3$s" (for type "%1$s" and key "%2$s") failed validation!%4$s';
    const EXCEPT_MSG_OPERATION_VALUE_INVALID_FORMAT = 'Following operation value "%3$s" (for type "%1$s" and key "%2$s") failed validation!%4$s';



}