<?php
/**
 * Description :
 * This class allows to define operator browser class.
 * Can be consider is base of all operator browser type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\model;

use liberty_code\item_browser\browser\page\model\PageBrowser;
use liberty_code\item_browser\browser\operation\api\OperatorBrowserInterface;

use Exception;
use liberty_code\data\data\exception\KeyNotFoundException;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\operation\exception\OperationTypeInvalidFormatException;
use liberty_code\item_browser\browser\operation\exception\OperationConfigInvalidFormatException;
use liberty_code\item_browser\browser\operation\exception\OperationValueInvalidFormatException;



abstract class OperatorBrowser extends PageBrowser implements OperatorBrowserInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Operation configurations and values.
     * @var OperatorData
     */
    protected $objOperatorData;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param OperatorData $objOperatorData
     * @param array $tabOperationConfig = array()
     * @param array $tabOperationValue = array()
     */
	public function __construct(
        OperatorData $objOperatorData,
        RegisterInterface $objCacheRegister = null,
        array $tabConfig = null,
        array $tabOperationConfig = array(),
        array $tabOperationValue = array()
    )
	{
	    // Init var
        $this->objOperatorData = $objOperatorData;
        $this->objOperatorData->setDataSrc(array());

		// Call parent constructor
		parent::__construct($objCacheRegister, $tabConfig);

        // Init operation configurations
        foreach($tabOperationConfig as $type => $config)
        {
            $this->hydrateOperationConfig($type, $config, true, true);
        }

        // Init operation values
        foreach($tabOperationValue as $type => $value)
        {
            $this->hydrateOperationValue($type, $value, true, true);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function hydrateOperationConfig(
        $strType,
        array $tabConfig = array(),
        $boolOverwrite = true,
        $boolClear = true
    )
    {
        // Init var
        $result = true;
        $boolOverwrite = is_bool($boolOverwrite) ? $boolOverwrite : true;
        $boolClear = is_bool($boolClear) ? $boolClear : true;

        // Clear all configuration if required
        if($boolClear)
        {
            $this->removeOperationConfigAll($strType);
        }

        // Run each configuration
        foreach($tabConfig as $strKey => $config)
        {
            // Get info
            $boolSet = (
                $boolOverwrite ||
                (!$this->checkOperationConfigExists($strType, $strKey))
            );

            // Register configuration, if required
            if($boolSet)
            {
                $this->setOperationConfig($strType, $strKey, $config);
            }

            $result = $result && $boolSet;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function hydrateOperationValue(
        $strType,
        array $tabValue = array(),
        $boolOverwrite = true,
        $boolClear = true
    )
    {
        // Init var
        $result = true;
        $boolOverwrite = is_bool($boolOverwrite) ? $boolOverwrite : true;
        $boolClear = is_bool($boolClear) ? $boolClear : true;

        // Clear all values if required
        if($boolClear)
        {
            $this->removeOperationValueAll($strType);
        }

        // Run each values
        foreach($tabValue as $strKey => $value)
        {
            // Get info
            $boolSet = (
                $boolOverwrite ||
                (!$this->checkOperationValueExists($strType, $strKey))
            );

            // Register value, if required
            if($boolSet)
            {
                $this->setOperationValue($strType, $strKey, $value);
            }

            $result = $result && $boolSet;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified operation type is valid.
     * Overwrite it to set specific rules.
     *
     * @param string $strType
     * @param string|Exception &$error = null
     * @return boolean
     */
    protected function checkOperationTypeValid($strType, &$error = null)
    {
        // Return result
        return true;
    }



    /**
     * Check if specified operation configuration is valid,
     * for specified operation type
     * and specified operation key.
     * Overwrite it to set specific rules.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $config
     * @param string|Exception &$error = null
     * @return boolean
     */
    protected function checkOperationConfigValid($strType, $strKey, $config, &$error = null)
    {
        // Return result
        return true;
    }



    /**
     * Check if specified operation value is valid,
     * for specified operation type
     * and specified operation key.
     * Overwrite it to set specific rules.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $value
     * @param string|Exception &$error = null
     * @return boolean
     */
    protected function checkOperationValueValid($strType, $strKey, $value, &$error = null)
    {
        // Return result
        return true;
    }



    /**
     * Set validation for specified operation type.
     *
     * @param string $strType
     * @throws Exception
     */
    protected function setValidOperationType($strType)
    {
        // Init var
        $error = null;

        // Throw exception if validation not pass
        if(!$this->checkOperationTypeValid($strType, $error))
        {
            // Throw custom exception
            if($error instanceof Exception)
            {
                throw $error;
            }
            // Throw standard exception
            else
            {
                throw new OperationTypeInvalidFormatException($strType, $error);
            }
        }
    }



    /**
     * Set validation for specified operation configuration,
     * for specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $config
     * @throws Exception
     */
    protected function setValidOperationConfig($strType, $strKey, $config)
    {
        // Init var
        $error = null;

        // Throw exception if validation not pass
        if(!$this->checkOperationConfigValid($strType, $strKey, $config, $error))
        {
            // Throw custom exception
            if($error instanceof Exception)
            {
                throw $error;
            }
            // Throw standard exception
            else
            {
                throw new OperationConfigInvalidFormatException($strType, $strKey, $config, $error);
            }
        }
    }



    /**
     * Set validation for specified operation value,
     * for specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $value
     * @throws Exception
     */
    protected function setValidOperationValue($strType, $strKey, $value)
    {
        // Init var
        $error = null;

        // Throw exception if validation not pass
        if(!$this->checkOperationValueValid($strType, $strKey, $value, $error))
        {
            // Throw custom exception
            if($error instanceof Exception)
            {
                throw $error;
            }
            // Throw standard exception
            else
            {
                throw new OperationValueInvalidFormatException($strType, $strKey, $value, $error);
            }
        }
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function checkOperationConfigExists($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathConfig($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);

        // Return result
        return $this->objOperatorData->checkValueExists($strPath);
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function checkOperationValueExists($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathValue($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);

        // Return result
        return $this->objOperatorData->checkValueExists($strPath);
    }



    /**
     * Set check if the specified operator data path exists.
     *
     * @param string $strPath
     * @throws KeyNotFoundException
     */
    protected function setCheckOperatorPathExists($strPath)
    {
        // Throw exception, if not found
        if(!$this->objOperatorData->checkValueExists($strPath))
        {
            throw new KeyNotFoundException($strPath);
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of string operation types filled.
     *
     * @inheritdoc
     */
    public function getTabOperationType()
    {
        // Return result
        return $this->objOperatorData->getTabType();
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function getTabOperationConfig($strType)
    {
        // Check arguments
        $this->setValidOperationType($strType);

        // Init var
        $result = array();
        $tabKey = $this->objOperatorData->getTabTypeKey($strType);

        // Run all operation keys
        foreach($tabKey as $strKey)
        {
            // Register operation configuration, if found
            if($this->checkOperationConfigExists($strType, $strKey))
            {
                $result[$strKey] = $this->getOperationConfig($strType, $strKey);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function getOperationConfig($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathConfig($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);
        $this->setCheckOperatorPathExists($strPath);

        // Return result
        return $this->objOperatorData->getValue($strPath);
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function getTabOperationValue($strType)
    {
        // Check arguments
        $this->setValidOperationType($strType);

        // Init var
        $result = array();
        $tabKey = $this->objOperatorData->getTabTypeKey($strType);

        // Run all operation keys
        foreach($tabKey as $strKey)
        {
            // Register operation value, if found
            if($this->checkOperationValueExists($strType, $strKey))
            {
                $result[$strKey] = $this->getOperationValue($strType, $strKey);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function getOperationValue($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathValue($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);
        $this->setCheckOperatorPathExists($strPath);

        // Return result
        return $this->objOperatorData->getValue($strPath);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function setOperationConfig($strType, $strKey, $config)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathConfig($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);
        $this->setValidOperationConfig($strType, $strKey, $config);

        // Setting configuration
        $result = $this->objOperatorData->putValue($strPath, $config);

        // Clear all cache, if required
        if($result)
        {
            $this->removeCacheAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function setOperationValue($strType, $strKey, $value)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathValue($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);
        $this->setValidOperationValue($strType, $strKey, $value);

        // Setting value
        $result = $this->objOperatorData->putValue($strPath, $value);

        // Clear all cache, if required
        if($result)
        {
            $this->removeCacheAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function removeOperationConfig($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathConfig($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);

        // Removing configuration
        $result = $this->objOperatorData->removeValue($strPath);

        // Clear all cache, if required
        if($result)
        {
            $this->removeCacheAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function removeOperationConfigAll($strType)
    {
        // Init var
        $result = true;
        $tabKey = array_keys($this->getTabOperationConfig($strType));

        // Run all configurations
        foreach($tabKey as $strKey)
        {
            // Remove configuration
            $result = $this->removeOperationConfig($strType, $strKey) && $result;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function removeOperationValue($strType, $strKey)
    {
        // Init var
        $strPath = $this->objOperatorData->getStrPathValue($strType, $strKey);

        // Check arguments
        $this->setValidOperationType($strType);

        // Removing value
        $result = $this->objOperatorData->removeValue($strPath);

        // Clear all cache, if required
        if($result)
        {
            $this->removeCacheAll();
        }

        // Return result: Removing value
        return $result;
    }



    /**
     * @inheritdoc
     * @throws Exception
     */
    public function removeOperationValueAll($strType)
    {
        // Init var
        $result = true;
        $tabKey = array_keys($this->getTabOperationValue($strType));

        // Run all configurations
        foreach($tabKey as $strKey)
        {
            // Remove configuration
            $result = $this->removeOperationValue($strType, $strKey) && $result;
        }

        // Return result
        return $result;
    }



}