<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\sort\library;



class ConstSortBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Operation type configuration
    const OPERATION_TYPE_SORT = 'sort';



    // Exception message constants
    const EXCEPT_MSG_OPERATION_TYPE_INVALID_FORMAT = 'Operation type allowed: "sort".';
}