<?php
/**
 * Description :
 * This class allows to define sort browser class.
 * Sort browser is automatic operator browser, that uses specified sort operations,
 * as browsing configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\sort\model;

use liberty_code\item_browser\browser\operation\auto\model\AutoOperatorBrowser;

use liberty_code\item_browser\browser\operation\sort\library\ConstSortBrowser;



/**
 * @method boolean hydrateSortConfig(array $tabConfig = array(), boolean $boolOverwrite = true, boolean $boolClear = true) Hydrate sort operation configurations.
 * @see AutoOperatorBrowser::hydrateOperationConfig()
 *
 * @method boolean hydrateSortValue(array $tabValue = array(), boolean $boolOverwrite = true, boolean $boolClear = true) Hydrate sort operation values.
 * @see AutoOperatorBrowser::hydrateOperationValue()
 *
 * @method boolean checkSortConfigExists(string $strKey) Check if the specified sort operation configuration exists.
 * @see AutoOperatorBrowser::checkOperationConfigExists()
 *
 * @method boolean checkSortValueExists(string $strKey) Check if the specified sort operation value exists.
 * @see AutoOperatorBrowser::checkOperationValueExists()
 *
 * @method array getTabSortConfig() Get all sort operation configurations.
 * @see AutoOperatorBrowser::getTabOperationConfig()
 *
 * @method array getSortConfig(string $strKey) Get sort operation configuration.
 * @see AutoOperatorBrowser::getOperationConfig()
 *
 * @method array getTabSortValue() Get all sort operation values.
 * @see AutoOperatorBrowser::getTabOperationValue()
 *
 * @method array getSortValue(string $strKey) Get sort operation value.
 * @see AutoOperatorBrowser::getOperationValue()
 *
 * @method array setSortConfig(string $strKey, mixed $config) Set specified sort operation configuration.
 * @see AutoOperatorBrowser::setOperationConfig()
 *
 * @method array setSortValue(string $strKey, mixed $value) Set specified sort operation value.
 * @see AutoOperatorBrowser::setOperationValue()
 *
 * @method array removeSortConfig(string $strKey) Remove specified sort operation configuration.
 * @see AutoOperatorBrowser::removeOperationConfig()
 *
 * @method array removeSortConfigAll() Remove all sort operation configurations.
 * @see AutoOperatorBrowser::removeOperationConfigAll()
 *
 * @method array removeSortValue(string $strKey) Remove specified sort operation value.
 * @see AutoOperatorBrowser::removeOperationValue()
 *
 * @method array removeSortValueAll() Remove all sort operation values.
 * @see AutoOperatorBrowser::removeOperationValueAll()
 */
abstract class SortBrowser extends AutoOperatorBrowser
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkOperationTypeValid($strType, &$error = null)
    {
        // Init var
        $result = ($strType == ConstSortBrowser::OPERATION_TYPE_SORT);

        // Set error message, if check not pass
        if(!$result)
        {
            $error = ConstSortBrowser::EXCEPT_MSG_OPERATION_TYPE_INVALID_FORMAT;
        }

        // Return result
        return $result;
    }



}