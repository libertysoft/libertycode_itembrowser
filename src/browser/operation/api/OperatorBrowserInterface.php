<?php
/**
 * Description :
 * This class allows to describe behavior of operator browser class.
 * Operator browser is browser, that uses specified configured operations
 * and specified operations values, as browsing configuration.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\browser\operation\api;

use liberty_code\item_browser\browser\api\BrowserInterface;



interface OperatorBrowserInterface extends BrowserInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate operation configurations from specified associative array of configurations.
     * Format: key: string key => value: mixed configuration.
     *
     * @param string $strType
     * @param array $tabConfig = array()
     * @param boolean $boolOverwrite = true
     * @param boolean $boolClear = true
     * @return boolean: true if correctly hydrated, false else
     */
    public function hydrateOperationConfig(
        $strType,
        array $tabConfig = array(),
        $boolOverwrite = true,
        $boolClear = true
    );



    /**
     * Hydrate operation values from specified associative array of values.
     * Format: key: string key => value: mixed value.
     *
     * @param string $strType
     * @param array $tabValue = array()
     * @param boolean $boolOverwrite = true
     * @param boolean $boolClear = true
     * @return boolean: true if correctly hydrated, false else
     */
    public function hydrateOperationValue(
        $strType,
        array $tabValue = array(),
        $boolOverwrite = true,
        $boolClear = true
    );





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified operation configuration exists.
     *
     * @param string $strType
     * @param string $strKey
     * @return boolean
     */
    public function checkOperationConfigExists($strType, $strKey);



    /**
     * Check if the specified operation value exists.
     *
     * @param string $strType
     * @param string $strKey
     * @return boolean
     */
    public function checkOperationValueExists($strType, $strKey);





	// Methods getters
	// ******************************************************************************

    /**
     * Get index array of string operation types.
     *
     * @return array
     */
    public function getTabOperationType();



    /**
     * Get all operation configurations,
     * from specified operation type.
     * Return format: key: string key => value: mixed config.
     *
     * @param string $strType
     * @return array
     */
    public function getTabOperationConfig($strType);



    /**
     * Get operation configuration,
     * from specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @return mixed
     */
    public function getOperationConfig($strType, $strKey);



    /**
     * Get all operation values,
     * from specified operation type.
     * Return format: key: string key => value: mixed value.
     *
     * @param string $strType
     * @return array
     */
    public function getTabOperationValue($strType);



    /**
     * Get operation value,
     * from specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @return mixed
     */
    public function getOperationValue($strType, $strKey);





    // Methods setters
    // ******************************************************************************

    /**
     * Set specified operation configuration (update if exists, add else),
     * for specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $config
     * @return boolean: true if correctly set, false else
     */
    public function setOperationConfig($strType, $strKey, $config);



    /**
     * Set specified operation value (update if exists, add else),
     * for specified operation type
     * and specified operation key.
     * Configuration must be set previously.
     *
     * @param string $strType
     * @param string $strKey
     * @param mixed $value
     * @return boolean: true if correctly set, false else
     */
    public function setOperationValue($strType, $strKey, $value);



    /**
     * Remove specified operation configuration.
     * It remove potential associated operation value.
     *
     * @param string $strType
     * @param string $strKey
     * @return boolean: true if correctly removed, false else
     */
    public function removeOperationConfig($strType, $strKey);



    /**
     * Remove all operation configurations,
     * from specified operation type.
     *
     * @param string $strType
     * @return boolean: true if correctly removed, false else
     */
    public function removeOperationConfigAll($strType);



    /**
     * Remove specified operation value.
     *
     * @param string $strType
     * @param string $strKey
     * @return boolean: true if correctly removed, false else
     */
    public function removeOperationValue($strType, $strKey);



    /**
     * Remove all operation values,
     * from specified operation type.
     *
     * @param string $strType
     * @return boolean: true if correctly removed, false else
     */
    public function removeOperationValueAll($strType);
}