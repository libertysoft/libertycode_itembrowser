<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/browser/operation/test/CriteriaBrowserTest.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\library\ConstBrowser;

// Use test
use liberty_code\item_browser\browser\operation\test\CriteriaBrowserTest;



// Init var
$tabConfig = array(
    ConstBrowser::TAB_CONFIG_KEY_QUERY => []
);
$objRegister = new MemoryRegister();
$objOperatorData = new OperatorData();
$objCriteriaBrowser = new CriteriaBrowserTest(
    $objOperatorData,
    $objRegister,
    $tabConfig,
    array(),
    array()
);



// Test hydrate xyz config
echo('Test hydrate xyz config: <br />');
$tabConfig = array(
    'key_xyz_1' => 'Xyz config 1',
    'key_xyz_2' => 'Xyz config 2',
    'key_xyz_3' => 'Xyz config 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateXyzConfig($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

echo('<br /><br /><br />');



// Test hydrate xyz value
echo('Test hydrate xyz value: <br />');
$tabConfig = array(
    'key_xyz_1' => 'Xyz value 1',
    'key_xyz_2' => 'Xyz value 2',
    'key_xyz_3' => 'Xyz value 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateXyzValue($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

echo('<br /><br /><br />');

echo('Operation type :<pre>');var_dump($objCriteriaBrowser->getTabOperationType());echo('</pre>');
//echo('Operation config :<pre>');var_dump($objCriteriaBrowser->getTabOperationConfig('xyz'));echo('</pre>');
//echo('Operation value :<pre>');var_dump($objCriteriaBrowser->getTabOperationValue('xyz'));echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate sort config
echo('Test hydrate sort config: <br />');
$tabConfig = array(
    'key_sort_1' => 'Sort config 1',
    'key_sort_2' => 'Sort config 2',
    'key_sort_3' => 'Sort config 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateSortConfig($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

foreach($objCriteriaBrowser->getTabSortConfig() as $key => $config)
{
    echo('Config for "' . $key . '":<pre>');var_dump($objCriteriaBrowser->getSortConfig($key));echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate sort value
echo('Test hydrate sort value: <br />');
$tabConfig = array(
    'key_sort_1' => 'Sort value 1',
    'key_sort_2' => 'Sort value 2',
    'key_sort_3' => 'Sort value 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateSortValue($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

foreach($objCriteriaBrowser->getTabSortValue() as $key => $config)
{
    echo('Value for "' . $key . '":<pre>');var_dump($objCriteriaBrowser->getSortValue($key));echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate criteria config
echo('Test hydrate criteria config: <br />');
$tabConfig = array(
    'key_crit_1' => 'Crit config 1',
    'key_crit_2' => 'Crit config 2',
    'key_crit_3' => 'Crit config 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateCriteriaConfig($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

$tabConfig = array(
    'key_crit_4' => 'Crit config 4',
    'key_crit_3' => 'Crit config 3 updated', // Not hydrated, config found
    'key_crit_5' => 'Crit config 5'
);
try{
    var_dump($objCriteriaBrowser->hydrateCriteriaConfig($tabConfig, false, false));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

foreach($objCriteriaBrowser->getTabCriteriaConfig() as $key => $config)
{
    echo('Config for "' . $key . '":<pre>');var_dump($config);echo('</pre>');
}

echo('<br /><br /><br />');



// Test hydrate criteria value
echo('Test hydrate criteria value: <br />');
$tabConfig = array(
    'key_crit_1' => 'Crit value 1',
    'key_crit_2' => 'Crit value 2',
    'key_crit_3' => 'Crit value 3'
);
try{
    var_dump($objCriteriaBrowser->hydrateCriteriaValue($tabConfig));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

$tabConfig = array(
    'key_crit_2' => 'Crit value 2 updated', // Not hydrated, value found
    'key_crit_4' => 'Crit value 4',
    'key_crit_6' => 'Crit value 6', // Ko: config not found
    'key_crit_5' => 'Crit value 5'
);
try{
    var_dump($objCriteriaBrowser->hydrateCriteriaValue($tabConfig, false, false));
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

foreach($objCriteriaBrowser->getTabCriteriaValue() as $key => $config)
{
    echo('Value for "' . $key . '":<pre>');var_dump($config);echo('</pre>');
}

echo('<br /><br /><br />');



// Test set criteria config
echo('Test set criteria config: <br />');
$tabData = array(
    'key_crit_1' => 'Crit config 1 updated', // Ok
    '' => 'Crit config 2 updated', // Ko: Bad path format
    'key_crit_6' => 'Crit config 6', // Ok
    7 => 'Crit config 7' // Ko: Bad path format
);

foreach($tabData as $key => $config)
{
    echo('Test set criteria config for "' . $key . '": <br />');
    try{
        echo('Set:<pre>');var_dump($objCriteriaBrowser->setCriteriaConfig($key, $config));echo('</pre>');
        echo('Check exists:<pre>');var_dump($objCriteriaBrowser->checkCriteriaConfigExists($key));echo('</pre>');
        echo('Get:<pre>');var_dump($objCriteriaBrowser->getCriteriaConfig($key));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('Criteria config :<pre>');var_dump($objCriteriaBrowser->getTabCriteriaConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test set criteria value
echo('Test set criteria value: <br />');
$tabData = array(
    'key_crit_1' => 'Crit value 1 updated', // Ok
    '' => 'Crit value 2 updated', // Ko: Bad path format
    'key_crit_6' => 'Crit value 6', // Ok
    'key_crit_7' => 'Crit value 7' // Ko: Config not found
);

foreach($tabData as $key => $value)
{
    echo('Test set criteria value for "' . $key . '": <br />');
    try{
        echo('Set:<pre>');var_dump($objCriteriaBrowser->setCriteriaValue($key, $value));echo('</pre>');
        echo('Check exists:<pre>');var_dump($objCriteriaBrowser->checkCriteriaValueExists($key));echo('</pre>');
        echo('Get:<pre>');var_dump($objCriteriaBrowser->getCriteriaValue($key));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('Criteria value :<pre>');var_dump($objCriteriaBrowser->getTabCriteriaValue());echo('</pre>');

echo('<br /><br /><br />');



// Test remove all criteria config
echo('Test remove all criteria config: <br />');
try{
    echo('Remove:<pre>');var_dump($objCriteriaBrowser->removeCriteriaConfigAll());echo('</pre>');
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

echo('Criteria config :<pre>');var_dump($objCriteriaBrowser->getTabCriteriaConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test remove all criteria value
echo('Test remove all criteria value: <br />');
try{
    echo('Remove:<pre>');var_dump($objCriteriaBrowser->removeCriteriaValueAll());echo('</pre>');
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}

echo('Criteria value :<pre>');var_dump($objCriteriaBrowser->getTabCriteriaValue());echo('</pre>');

echo('<br /><br /><br />');


