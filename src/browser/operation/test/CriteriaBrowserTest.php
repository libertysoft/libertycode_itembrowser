<?php

namespace liberty_code\item_browser\browser\operation\test;

use liberty_code\item_browser\browser\operation\criteria\model\CriteriaBrowser;



class CriteriaBrowserTest extends CriteriaBrowser
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /** @var array */
    protected $tabData = array(
        [
            'key_1' => 1,
            'key_2' => 'Value 1',
            'key_3' => true,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 2,
            'key_2' => 'Value 2',
            'key_3' => false,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 3,
            'key_2' => 'Value 3',
            'key_3' => true,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 4,
            'key_2' => 'Value 4',
            'key_3' => false,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 5,
            'key_2' => 'Value 5',
            'key_3' => true,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 6,
            'key_2' => 'Value 6',
            'key_3' => false,
            'key_4' => 'Value A'
        ],
        [
            'key_1' => 7,
            'key_2' => 'Value 7',
            'key_3' => true,
            'key_4' => 'Value B'
        ],
        [
            'key_1' => 8,
            'key_2' => 'Value 8',
            'key_3' => false,
            'key_4' => 'Value B'
        ],
        [
            'key_1' => 9,
            'key_2' => 'Value 9',
            'key_3' => true,
            'key_4' => 'Value B'
        ],
        [
            'key_1' => 10,
            'key_2' => 'Value 10',
            'key_3' => false,
            'key_4' => 'Value B'
        ],
        [
            'key_1' => 11,
            'key_2' => 'Value 11',
            'key_3' => true,
            'key_4' => 'Value B'
        ]
    );





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of items, from specified query.
     *
     * @return array
     */
    protected function getTabItemFromQuery()
    {
        // Init var
        $result = array();
        $query = $this->getQuery();

        if(is_array($query))
        {
            // Run all data
            foreach($this->tabData as $data)
            {
                // Check data matches with query
                $boolMatch = true;
                $tabQueryKey = array_keys($query);
                for($intCpt = 0; ($intCpt < count($tabQueryKey)) && $boolMatch; $intCpt++)
                {
                    $strQueryKey = $tabQueryKey[$intCpt];
                    $strQueryValue = $query[$strQueryKey];
                    $boolMatch = (
                        array_key_exists($strQueryKey, $data) &&
                        ($data[$strQueryKey] == $strQueryValue)
                    );
                }

                // Register data, if required (matches with query)
                if($boolMatch)
                {
                    $result[] = $data;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getIntItemCountEngine()
    {
        // Return result
        return count($this->getTabItemFromQuery());
    }



    /**
     * @inheritdoc
     */
    public function getTabItem()
    {
        // Init var
        $result = array_slice(
            $this->getTabItemFromQuery(),
            ($this->getIntActivePageIndex() * $this->getIntItemCountPerPage()),
            $this->getIntItemCountOnActivePage()
        );

        // Return result
        return $result;
    }



}