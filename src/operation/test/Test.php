<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\item_browser\operation\model\OperatorData;



// Init var
$tabDataSrc = array(
	'type 1' => [
	    'key 1' => [
	        'config' => 'config for type 1, key 1'
        ],
        'key 2' => [
            'config' => 'config for type 1, key 2',
            'value' => 'value for type 1, key 2'

        ]
    ]
);

$objData = new OperatorData();
$objData->setDataSrc($tabDataSrc);



// Test put
$tabPath = array(
    'type 2/key 1/value' => 'value for type 2, key 1', // Ko: no config found
    'type 2/key 1/config' => 'config for type 2, key 1', // Ok
    'type 2 key 2/config' => 'config for type 2, key 2', // Ko: path invalid
    'type 2/key 2/config' => 'config for type 2, key 2', // Ok
    'type 2/key 2/value/test' => 'value for type 2, key 2', // Ko: path invalid
    'type 2/key 2/value' => 'value for type 2, key 2', // Ok
    'type 3/key 1/configs' => 'config for type 3, key 1', // Ko: path invalid
    'type 3/key 1/value' => 'value for type 3, key 1', // Ko: no config found
);

foreach($tabPath as $strPath => $value)
{
	echo('Test put "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->putValue($strPath, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test list operation types
echo('Test list operation types: <br />');
echo('<pre>');print_r($objData->getTabType());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test list operation keys
$tabType = array(
    'type 1', // Ok
    'type 2', // Ok
    'type 3', // Ko: not found
    'type 4' // Ko: not found
);

foreach($tabType as $strType)
{
    echo('Test list operation keys, for type "'.$strType.'": <br />');
    try{
        echo('<pre>');var_dump($objData->getTabTypeKey($strType));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test check, get
$tabPath = array(
    'type 1/key 1/config', // Ok
    'type 1/key 1/value', // Ko: not found
    'type 1/key 2/config', // Ok
    'type 1/key 2/value', // Ok
    'type 1 key 2/value', // Ko: path invalid
    'type 2/key 1/config', // Ok
    'type 2/key 1/value', // Ko: not found
    'type 2/key 2/config', // Ok
    'type 2/key 2/config/test', // Ko: path invalid
    'type 2/key 2/value' // Ok
);

foreach($tabPath as $strPath)
{
    echo('Test check, get "'.$strPath.'": <br />');
    try{
        if($objData->checkValueExists($strPath))
        {
            echo('<pre>');var_dump($objData->getValue($strPath));echo('</pre>');
        }
        else
        {
            echo('Key not found<br />');
            $objData->getValue($strPath);
        }
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test remove
$tabPath = array(
    'type 1/key 1/config', // Ok
    'type 1/key 1/value', // Ko: not found
    'type 1 key 2/config', // Ko: path invalid
    'type 1/key 2/config', // Ok
    'type 1/key 2/value', // Ko: Already deleted
    'type 2/key 2/value', // Ok
    'type 2/key 2/config' // Ok
);

foreach($tabPath as $strPath)
{
	echo('Test remove "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->removeValue($strPath));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


