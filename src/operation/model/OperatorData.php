<?php
/**
 * Description :
 * This class allows to define operator data.
 * Operator data allows to store operations configurations
 * and operations values, in specified data source.
 *
 * Operator data uses the following specified configuration:
 * [
 *     Path table configuration,
 *
 *     path_separator(optional: got '/', if not found):
 *         'String path separator'
 * ]
 *
 * Data source is array, with following format:
 * [
 *     'operation type 1' => [
 *         'operation key 1' => [
 *             config => mixed operation configuration 1,
 *             value => mixed operation value 1
 *         ],
 *         ...,
 *         'operation key N' => ...
 *     ],
 *     ...,
 *     'operation type N' => ...
 * ]
 *
 * Array path follows following format:
 * - For operation configuration: 'operation type/operation key/config'
 * - For operation value: 'operation type/operation key/value'
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\operation\model;

use liberty_code\data\data\table\path\model\PathTableData;

use liberty_code\data\data\table\path\library\ConstPathTableData;
use liberty_code\item_browser\operation\library\ConstOperatorData;
use liberty_code\item_browser\operation\exception\PathInvalidFormatException;
use liberty_code\item_browser\operation\exception\PathConfigNotFoundException;



class OperatorData extends PathTableData
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValidKey($strPath, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            PathInvalidFormatException::setCheck($strPath, $this->getStrConfigPathSeparator());
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkValidValue($strPath, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Case operation value
            if($this->checkPathIsValue($strPath))
            {
                $strType = $this->getStrTypeFromPath($strPath);
                $strKey = $this->getStrKeyFromPath($strPath);
                $strPathConfig = $this->getStrPathConfig($strType, $strKey);

                // Check operation configuration exists
                if(!$this->checkValueExists($strPathConfig))
                {
                    throw new PathConfigNotFoundException($strPathConfig);
                }
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods events
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function onAfterRemoveValue($strPath)
    {
        // Init var
        $strPathSeparator = $this->getStrConfigPathSeparator();
        $strType = $this->getStrTypeFromPath($strPath);
        $strKey = $this->getStrKeyFromPath($strPath);

        // Case operation configuration
        if($this->checkPathIsConfig($strPath))
        {
            $strType = $this->getStrTypeFromPath($strPath);
            $strKey = $this->getStrKeyFromPath($strPath);
            $strPathValue = $this->getStrPathValue($strType, $strKey);

            // Remove operation value, if required
            if($this->checkValueExists($strPathValue))
            {
                $this->removeValue($strPathValue);
            }
        }

        // Remove key, if required
        $strPathKey = $strType . $strPathSeparator . $strKey;
        $value = parent::getValueEngine($strPathKey);
        if(is_array($value) && (count($value) == 0))
        {
            parent::removeValueEngine($strPathKey);
        }

        // Remove type, if required
        $value = parent::getValueEngine($strType);
        if(is_array($value) && (count($value) == 0))
        {
            parent::removeValueEngine($strType);
        }
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified path is for operation configuration.
     *
     * @param string $strPath
     * @return boolean
     */
    public function checkPathIsConfig($strPath)
    {
        // Init var
        $strPathSeparator = $this->getStrConfigPathSeparator();
        $strPathPatternConfig = ConstOperatorData::getStrPathPatternRegexpConfig($strPathSeparator);

        // Return result
        return (
            is_string($strPath) &&
            (preg_match($strPathPatternConfig, $strPath) == 1)
        );
    }



    /**
     * Check if specified path for is operation value.
     *
     * @param string $strPath
     * @return boolean
     */
    public function checkPathIsValue($strPath)
    {
        // Init var
        $strPathSeparator = $this->getStrConfigPathSeparator();
        $strPathPatternValue = ConstOperatorData::getStrPathPatternRegexpValue($strPathSeparator);

        // Return result
        return (
            is_string($strPath) &&
            (preg_match($strPathPatternValue, $strPath) == 1)
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrConfigPathSeparator()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = ConstOperatorData::CONFIG_DEFAULT_PATH_SEPARATOR;

        // Get path separator, if found
        if(array_key_exists(ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR, $tabConfig))
        {
            $result = $tabConfig[ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR];
        }

        // Return result
        return $result;
    }



    /**
     * Get specified matched element,
     * from specified path.
     *
     * @param string $strPath
     * @param integer $intIndex
     * @return null|string
     * @throws PathInvalidFormatException
     */
    protected function getStrElmFromPath($strPath, $intIndex = 0)
    {
        // Init var
        $result = null;
        $intIndex = ((is_int($intIndex) && ($intIndex >= 0)) ? $intIndex : 0);
        $intIndex++;
        $strPathSeparator = $this->getStrConfigPathSeparator();

        // Check argument
        PathInvalidFormatException::setCheck($strPath, $strPathSeparator);

        // Get path pattern
        $strPathPattern = ConstOperatorData::getStrPathPatternRegexpConfig($strPathSeparator);
        if($this->checkPathIsValue($strPath))
        {
            $strPathPattern = ConstOperatorData::getStrPathPatternRegexpValue($strPathSeparator);
        }

        // Get matched element
        $tabMatch = array();
        if(
            (preg_match($strPathPattern, $strPath, $tabMatch) == 1) &&
            ($intIndex < count($tabMatch))
        )
        {
            $result = $tabMatch[$intIndex];
        }

        // Return result
        return $result;
    }



    /**
     * Get operation type, from specified path.
     *
     * @param string $strPath
     * @return string
     * @throws PathInvalidFormatException
     */
    public function getStrTypeFromPath($strPath)
    {
        // Return result
        return $this->getStrElmFromPath($strPath, 0);
    }



    /**
     * Get operation key, from specified path.
     *
     * @param string $strPath
     * @return string
     * @throws PathInvalidFormatException
     */
    public function getStrKeyFromPath($strPath)
    {
        // Return result
        return $this->getStrElmFromPath($strPath, 1);
    }



    /**
     * Get operation object path,
     * from specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @param string $strObject
     * @return string
     * @throws PathInvalidFormatException
     */
    protected function getStrPath($strType, $strKey, $strObject)
    {
        // Init var
        $strPathSeparator = $this->getStrConfigPathSeparator();
        $result = sprintf(
            ConstOperatorData::CONF_PATH_PATTERN,
            $strPathSeparator,
            strval($strObject),
            strval($strType),
            strval($strKey)
        );

        // Check result
        PathInvalidFormatException::setCheck($result, $strPathSeparator);

        // Return result
        return $result;
    }



    /**
     * Get operation configuration path,
     * from specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @return string
     * @throws PathInvalidFormatException
     */
    public function getStrPathConfig($strType, $strKey)
    {
        // Return result
        return $this->getStrPath(
            $strType,
            $strKey,
            ConstOperatorData::CONF_PATH_CONFIG
        );
    }



    /**
     * Get operation value path,
     * from specified operation type
     * and specified operation key.
     *
     * @param string $strType
     * @param string $strKey
     * @return string
     */
    public function getStrPathValue($strType, $strKey)
    {
        // Return result
        return $this->getStrPath(
            $strType,
            $strKey,
            ConstOperatorData::CONF_PATH_VALUE
        );
    }



    /**
     * Get index array of string operation types.
     *
     * @return array
     */
    public function getTabType()
    {
        // Return result
        return array_keys($this->getDataSrc());
    }



    /**
     * Get index array of string operation configuration keys,
     * from specified operation type.
     *
     * @param string $strType
     * @return array
     */
    public function getTabTypeKey($strType)
    {
        // Init var
        $result = array();
        $typeValue = parent::getValueEngine($strType);

        // Get array of keys, if required
        if(is_array($typeValue))
        {
            $result = array_keys($typeValue);
        }

        // Return result
        return $result;
    }



}