<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\operation\library;



class ConstOperatorData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Default configuration
    const CONFIG_DEFAULT_PATH_SEPARATOR = '/';

    // Path configuration
    const CONF_PATH_PATTERN_REGEXP = '#^([^0-9%1$s][^%1$s]*)%1$s([^0-9%1$s][^%1$s]*)%1$s%2$s$#';
    const CONF_PATH_PATTERN = '%3$s%1$s%4$s%1$s%2$s';
    const CONF_PATH_CONFIG = 'config';
    const CONF_PATH_VALUE = 'value';



	// Exception message constants
    const EXCEPT_MSG_PATH_INVALID_FORMAT = 'Following path "%1$s" invalid! The path must be a valid string and following the operator data path standard.';
    const EXCEPT_MSG_PATH_CONFIG_NOT_FOUND = 'Following configuration path "%1s" not found!';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get REGEXP pattern for configuration path.
     *
     * @param string $strPathSeparator = self::CONFIG_DEFAULT_PATH_SEPARATOR
     * @return string
     */
    static public function getStrPathPatternRegexpConfig($strPathSeparator = self::CONFIG_DEFAULT_PATH_SEPARATOR)
    {
        // Return result
        return sprintf(
            static::CONF_PATH_PATTERN_REGEXP,
            preg_quote(strval($strPathSeparator)),
            preg_quote(static::CONF_PATH_CONFIG)
        );
    }



    /**
     * Get REGEXP pattern for value path.
     *
     * @param string $strPathSeparator = self::DATA_DEFAULT_VALUE_PATH_SEPARATOR
     * @return string
     */
    static public function getStrPathPatternRegexpValue($strPathSeparator = self::CONFIG_DEFAULT_PATH_SEPARATOR)
    {
        // Return result
        return sprintf(
            static::CONF_PATH_PATTERN_REGEXP,
            preg_quote(strval($strPathSeparator)),
            preg_quote(static::CONF_PATH_VALUE)
        );
    }



}