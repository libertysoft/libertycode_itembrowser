<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\operation\exception;

use liberty_code\item_browser\operation\library\ConstOperatorData;



class PathConfigNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $path
     */
	public function __construct($path)
	{
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf(
            ConstOperatorData::EXCEPT_MSG_PATH_CONFIG_NOT_FOUND,
            strval($path)
        );
	}
	
	
	
}