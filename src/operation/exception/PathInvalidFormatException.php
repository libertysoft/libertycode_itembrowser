<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\item_browser\operation\exception;

use liberty_code\item_browser\operation\library\ConstOperatorData;



class PathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $path
     */
	public function __construct($path)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstOperatorData::EXCEPT_MSG_PATH_INVALID_FORMAT, strval($path));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified path has valid format.
	 *
     * @param mixed $path
     * @param string $strPathSeparator = ConstPathTableData::DATA_DEFAULT_VALUE_PATH_SEPARATOR
	 * @return boolean
	 * @throws static
     */
    static public function setCheck($path, $strPathSeparator = ConstOperatorData::CONFIG_DEFAULT_PATH_SEPARATOR)
    {
		// Init var
        $strPathSeparator = (
            (is_string($strPathSeparator) && (trim($strPathSeparator) != '')) ?
                $strPathSeparator :
                ConstOperatorData::CONFIG_DEFAULT_PATH_SEPARATOR
        );
        $strPathPatternConfig = ConstOperatorData::getStrPathPatternRegexpConfig($strPathSeparator);
        $strPathPatternValue = ConstOperatorData::getStrPathPatternRegexpValue($strPathSeparator);
        $result =
            is_string($path) &&
            (
                (preg_match($strPathPatternConfig, $path) == 1) ||
                (preg_match($strPathPatternValue, $path) == 1)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($path);
		}
		
		// Return result
		return $result;
    }
	
	
	
}