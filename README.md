LibertyCode_ItemBrowser
=======================



Description
-----------

Library contains item browsing components. 
It allows to provide items, from selection query, 
using specific configuration to browse through result set.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/item_browser ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/item_browser": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Operator data

Handle array data allows to manage operation configurations and values items.

#### Browser

Browser allows to provide items, from specified selection query,
using specified configuration to browse through return items.

_Elements_

- Browser
    
    Allows to provide items, from specified selection query
    and from specified browsing configuration.
    Can be consider as base of all browser type.

- PageBrowser
    
    Extends default browser features. 
    Uses page browsing configuration, 
    to provide paginated result set of items.

- OperatorBrowser
    
    Extends default browser features. 
    Uses specified configured operations 
    and specified operations values, 
    as browsing configuration.
    
    Note: 
    Default operator browser extends 
    page and operator browser features.
    
- AutoOperatorBrowser
    
    Extends default operator browser features. 
    Allows to call customized methods, 
    to use and manage operations.
    
- SortBrowser
    
    Extends automatic operator browser features. 
    Uses specified sort operations, 
    as browsing configuration.
    
- CriteriaBrowser
    
    Extends sort browser features. 
    Uses specified criteria operations, 
    as browsing configuration.
    
_Example_

```php
// Define new page browser type
use liberty_code\item_browser\browser\page\model\PageBrowser;
class PageBrowserTest extends PageBrowser
{
    protected function getIntItemCountEngine()
    {
        return ...total count of item;
    }
    
    public function getTabItem()
    {
        return ...index array of items;
    }
}
...
// Get page browser
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\item_browser\browser\library\ConstBrowser;
$config = array(
    ConstBrowser::TAB_CONFIG_KEY_QUERY => []
);
$register = new MemoryRegister();
$pageBrowser = new PageBrowserTest($register, $config);
...
// Set number items per page
$pageBrowser->setItemCountPerPage(10);
...
$pageCount = $pageBrowser->getIntPageCount(); // Get number of pages
for($cpt = 0; $cpt < count($pageCount); $cpt++) {
    $pageBrowser->setActivePageIndex($cpt);
    var_dump($objPageBrowser->getTabItem());
}
/**
 * Show: 
 * index array of items, on page 1
 * ...
 * index array of items, on page N
 */
...
```

---


